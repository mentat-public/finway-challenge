import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";
import { act } from "react-dom/test-utils";

const mockOn = jest.fn();
const mockOff = jest.fn();
const mockEmit = jest.fn();

class mockWSClient {
  on = mockOn;
  off = mockOff;
  emit = mockEmit;
}

jest.mock("./network/wsclient", () => ({
  WSClient: mockWSClient,
}));

const { default: App } = require("./App");

describe("App", () => {
  it("has title", () => {
    render(<App />);
    const linkElement = screen.getByText(/Simple calculator/i);
    expect(linkElement).toBeInTheDocument();
  });

  it("supports operation command", () => {
    render(<App />);
    const input = screen.getByLabelText("Message") as HTMLInputElement;
    fireEvent.input(input, {
      target: { value: "operation 2+2" },
    });
    const form = screen.getByRole("form");
    expect(form).toBeDefined();
    expect(input.value).toBe("operation 2+2");
    fireEvent.submit(form);
    expect(mockEmit).toHaveBeenCalledWith("evaluate", "2+2");
  });

  it("supports history command", () => {
    render(<App />);
    const input = screen.getByLabelText("Message") as HTMLInputElement;
    fireEvent.input(input, {
      target: { value: "history" },
    });
    const form = screen.getByRole("form");
    expect(form).toBeDefined();
    expect(input.value).toBe("history");
    fireEvent.submit(form);
    expect(mockEmit).toHaveBeenCalledWith("history");
  });

  it("listens on incoming messages", () => {
    act(() => {
      render(<App />);
    });
    expect(mockOn.mock.calls.length).toBe(1);
    expect(mockOn.mock.calls[0][0]).toBe("text");
    const textHandler = mockOn.mock.calls[0][1];
    act(() => {
      textHandler("Hello buddy");
    });
    const messages = screen.getByLabelText("messages") as HTMLSpanElement;
    expect(messages).toBeDefined();
    expect(messages.textContent).toBe("Hello buddy\n");
  });
});
