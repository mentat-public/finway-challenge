const CLIENT_ID = "clientId";

export function getClientId(): string {
  let id = window.localStorage.getItem(CLIENT_ID);
  if (!id) {
    // Gnerate random id
    id = Math.random().toString(36).substring(12);
    window.localStorage.setItem(CLIENT_ID, id);
  }
  return id;
}
