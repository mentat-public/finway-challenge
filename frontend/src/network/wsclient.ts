import io from "socket.io-client";
import { getClientId } from "./id";

export class WSClient {
  private socket: SocketIOClient.Socket;

  constructor(url: string) {
    this.socket = io(url, {
      autoConnect: true,
      reconnection: true,
    });

    this.socket.on("connect", () => {
      this.socket.emit("auth", getClientId());
    });
  }

  emit(event: string, ...args: any[]) {
    this.socket.emit(event, ...args);
  }

  on(event: string, fn: Function) {
    this.socket.on(event, fn);
  }

  off(event: string, fn: Function) {
    this.socket.off(event, fn);
  }
}
