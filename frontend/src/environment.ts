declare global {
  interface Window {
    environment: Record<string, string>;
  }
}

export const BACKEND_URL =
  window.environment?.BACKEND_URL ?? "https://finway-api.mentatxx.com";
