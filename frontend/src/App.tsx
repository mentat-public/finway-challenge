import React, { ChangeEvent, FormEvent } from "react";
import "./App.scss";
import { BACKEND_URL } from "./environment";
import logo from "./logo.png";
import { WSClient } from "./network/wsclient";

interface AppState {
  chat: string;
  expression: string;
}

class App extends React.Component<{}, AppState> {
  state = {
    chat: "",
    expression: "",
  };

  client: WSClient;

  constructor(props: any) {
    super(props);
    this.client = new WSClient(BACKEND_URL);
  }

  commands = [
    {
      exp: /^\s*history\s*$/,
      handler: () => {
        this.client.emit("history");
      },
    },
    {
      exp: /^\s*operation\s+(.*?)\s*$/,
      handler: (g: string[] | null) => {
        if (g) {
          this.client.emit("evaluate", g[1]);
        }
      },
    },
  ];

  handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const command = this.commands.find((c) =>
      c.exp.test(this.state.expression)
    );
    if (command) {
      command.handler(command.exp.exec(this.state.expression));
      this.setState({ expression: "" });
    } else {
      this.setState((state) => ({
        chat: `Command unsupported\n${state.chat}`,
      }));
    }
  };

  handeExprChange = (e: ChangeEvent<HTMLInputElement>) =>
    this.setState({ expression: e.target.value });

  textHandler = (data: any) => {
    this.setState((state) => ({ chat: `${data}\n${state.chat}` }));
  };

  componentDidMount() {
    this.client.on("text", this.textHandler);
  }

  componentWillUnmount() {
    this.client.off("text", this.textHandler);
  }

  render() {
    const { chat, expression } = this.state;
    return (
      <div className="container">
        <div className="app">
          <header className="header">
            <img src={logo} className="header__logo" alt="logo" />
            <p className="header__title">Simple calculator</p>
          </header>
          <div className="calc">
            <form onSubmit={this.handleSubmit} role="form">
              <input
                autoFocus
                name="expression"
                aria-label="Message"
                className="input"
                value={expression}
                onChange={this.handeExprChange}
              />
            </form>
          </div>
          <pre className="chat">
            <p>Welcome to the calculator chat. Available commands are:</p>
            <ul>
              <li>
                <pre>operation 2+2</pre>
              </li>
              <li>
                <pre>history</pre>
              </li>
            </ul>
            <span aria-label="messages">{chat}</span>
          </pre>
        </div>
      </div>
    );
  }
}

export default App;
