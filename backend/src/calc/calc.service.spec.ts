import { Test, TestingModule } from '@nestjs/testing';
import { SyntaxError } from './calc-parser';
import { CalcService } from './calc.service';

describe('CalcService', () => {
  let service: CalcService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CalcService],
    }).compile();

    service = module.get<CalcService>(CalcService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('calculates expression with + and -', () => {
    expect(service.evaluate('10+5-3')).toBe(12);
  });

  it('calculates expression with correct operator priority', () => {
    expect(service.evaluate('10+5*3')).toBe(25);
  });

  it('support whitespaces between operators and symbols', () => {
    expect(service.evaluate('10 + 5 * 3')).toBe(25);
  });

  it('support parenthesis', () => {
    expect(service.evaluate('(10+5)*3')).toBe(45);
  });

  it('throws exception on invalid syntax', () => {
    expect(() => service.evaluate('azaza')).toThrow(SyntaxError);
  });
});
