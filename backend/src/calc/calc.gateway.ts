import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { CalcHistoryService } from './calc-history.service';
import { CalcService } from './calc.service';

@WebSocketGateway()
export class CalcGateway {
  @WebSocketServer()
  server: Server;

  constructor(private c: CalcService, private h: CalcHistoryService) {}

  @SubscribeMessage('auth')
  async auth(socket: Socket, data: any) {
    // set socket identifier
    socket.data = data;
  }

  @SubscribeMessage('evaluate')
  async evaluate(socket: Socket, data: any) {
    if (socket.data) {
      try {
        const expression = '' + data;
        const value = String(this.c.evaluate(expression));
        await this.h.addToHistory({ expression, value, clientId: socket.data });
        socket.emit('text', `${expression} = ${value}`);
      } catch (error) {
        socket.emit('text', error.message);
      }
    } else {
      socket.emit('text', 'Unauthenticated');
    }
  }

  @SubscribeMessage('history')
  async history(socket: Socket) {
    if (socket.data) {
      const history = await this.h.getHistory(socket.data);
      socket.emit(
        'text',
        history
          .map((i) => `${i.expression} = ${i.value}`)
          .reverse()
          .join('\n'),
      );
    } else {
      socket.emit('text', 'Unauthenticated');
    }
  }
}
