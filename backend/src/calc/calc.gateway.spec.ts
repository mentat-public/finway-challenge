import { Test, TestingModule } from '@nestjs/testing';
import { CalcHistoryService } from './calc-history.service';
import { SyntaxError } from './calc-parser';
import { CalcGateway } from './calc.gateway';
import { CalcService } from './calc.service';

describe('CalcGateway', () => {
  let gateway: CalcGateway;

  const mockCalcService = {
    evaluate: jest.fn(),
  };

  const mockCalcHistoryService = {
    addToHistory: jest.fn(),
    getHistory: jest.fn(),
  };
  const clientId = 'CLIENTID';

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CalcGateway,
        {
          provide: CalcService,
          useValue: mockCalcService,
        },
        {
          provide: CalcHistoryService,
          useValue: mockCalcHistoryService,
        },
      ],
    }).compile();

    gateway = module.get<CalcGateway>(CalcGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });

  it('gets client id', () => {
    const socket: any = {};
    gateway.auth(socket, clientId);
    expect(socket.data).toBe(clientId);
  });

  it('evaluates expression', async () => {
    const socket: any = { data: clientId, emit: jest.fn() };
    const expression = '2+2';
    mockCalcService.evaluate.mockReturnValueOnce(4);
    await gateway.evaluate(socket, expression);
    expect(mockCalcHistoryService.addToHistory).toHaveBeenCalledWith({
      expression: '2+2',
      value: '4',
      clientId,
    });
    expect(socket.emit).toHaveBeenCalledWith('text', '2+2 = 4');
  });

  it('returns error on wrong expression', async () => {
    const socket: any = { data: clientId, emit: jest.fn() };
    const expression = 'azaza';
    mockCalcService.evaluate.mockImplementation(() => {
      throw new SyntaxError('Invalid syntax');
    });
    await gateway.evaluate(socket, expression);

    expect(socket.emit).toHaveBeenCalledWith('text', 'Invalid syntax');
  });

  it('evaluate returns error on unauthenticacted', async () => {
    const socket: any = { data: undefined, emit: jest.fn() };
    const expression = '2+2';
    await gateway.evaluate(socket, expression);

    expect(socket.emit).toHaveBeenCalledWith('text', 'Unauthenticated');
  });

  it('history returns error on unauthenticacted', async () => {
    const socket: any = { data: undefined, emit: jest.fn() };
    await gateway.history(socket);

    expect(socket.emit).toHaveBeenCalledWith('text', 'Unauthenticated');
  });

  it('gets history', async () => {
    const socket: any = { data: clientId, emit: jest.fn() };
    mockCalcHistoryService.getHistory.mockReturnValue([
      {
        expression: '2*2',
        value: 4,
      },
      {
        expression: '3*3',
        value: 9,
      },
    ]);

    await gateway.history(socket);
    expect(mockCalcHistoryService.getHistory).toHaveBeenCalledWith(clientId);
    expect(socket.emit).toHaveBeenCalledWith('text', `3*3 = 9\n2*2 = 4`);
  });
});
