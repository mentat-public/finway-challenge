import { BadRequestException, Controller, Get, Query } from '@nestjs/common';
import { CalcService } from './calc.service';

@Controller('calc')
export class CalcController {
  constructor(private c: CalcService) {}

  @Get('/')
  calc(@Query('expression') expression: string) {
    try {
      return this.c.evaluate(expression);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }
}
