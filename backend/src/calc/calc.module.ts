import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HistoryItem, HistorySchema } from '../schemas/history.schema';
import { CalcHistoryService } from './calc-history.service';
import { CalcController } from './calc.controller';
import { CalcGateway } from './calc.gateway';
import { CalcService } from './calc.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: HistoryItem.name, schema: HistorySchema },
    ]),
  ],
  providers: [CalcGateway, CalcService, CalcHistoryService],
  controllers: [CalcController],
})
export class CalcModule {}
