import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import MockDate from 'mockdate';
import { HistoryDocument, HistoryItem } from '../schemas/history.schema';
import { CalcHistoryService } from './calc-history.service';

describe('CalcHistoryService', () => {
  let service: CalcHistoryService;
  const mockResponse: HistoryDocument[] = [
    {
      expression: '1+1',
      value: '2',
      clientId: 'CLIENTID',
      addedOn: new Date(),
    } as HistoryDocument,
  ];
  const mockFind = jest.fn(() => mockRepository).mockReturnThis();
  const mockSort = jest.fn(() => mockRepository).mockReturnThis();
  const mockLimit = jest.fn(() => mockRepository).mockReturnThis();
  const mockExec = jest.fn(() => Promise.resolve(mockResponse));
  const mockSave = jest.fn();

  const mockRepository: any = jest.fn().mockReturnValue({ save: mockSave });
  mockRepository.find = mockFind;
  mockRepository.sort = mockSort;
  mockRepository.limit = mockLimit;
  mockRepository.exec = mockExec;

  let dateNowSpy: jest.SpyInstance<number, []>;

  beforeAll(() => {
    // lock time
    MockDate.set(1487086800000);
  });

  afterAll(() => {
    // Unlock Time
    MockDate.reset();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CalcHistoryService,
        {
          provide: getModelToken(HistoryItem.name),
          useValue: mockRepository,
        },
      ],
    }).compile();

    service = module.get<CalcHistoryService>(CalcHistoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('gets history', async (done) => {
    expect(await service.getHistory('CLIENTID')).toEqual(mockResponse);
    done();
  });

  it('filters history by client id', () => {
    service.getHistory('CLIENTID');
    expect(mockRepository.find).toHaveBeenCalledWith({ clientId: 'CLIENTID' });
  });

  it('sort history by date', () => {
    service.getHistory('CLIENTID');
    expect(mockRepository.sort).toHaveBeenCalledWith({ addOn: 1 });
  });

  it('has limit on number of returned rows', () => {
    service.getHistory('CLIENTID');
    expect(mockRepository.limit).toHaveBeenCalledWith(10);
  });

  it('appends history', () => {
    const item = {
      expression: 'new expression',
      value: 'new value',
      clientId: 'CLIENTID',
    };
    service.addToHistory(item);

    expect(mockRepository).toHaveBeenCalledWith({
      addedOn: new Date(),
      ...item,
    });
    expect(mockSave).toHaveBeenCalled();
  });
});
