import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HistoryDocument, HistoryItem } from '../schemas/history.schema';

// Limit a number of retrieved records
const LIMIT = 10;

@Injectable()
export class CalcHistoryService {
  constructor(
    @InjectModel(HistoryItem.name) private historyModel: Model<HistoryDocument>,
  ) {}

  async getHistory(clientId: string) {
    return this.historyModel
      .find({ clientId })
      .sort({ addOn: 1 })
      .limit(LIMIT)
      .exec();
  }

  async addToHistory(
    item: Pick<HistoryItem, 'clientId' | 'expression' | 'value'>,
  ) {
    const n = new this.historyModel({
      ...item,
      addedOn: new Date(),
    });
    await n.save();
  }
}
