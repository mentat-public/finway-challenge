import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { SyntaxError } from './calc-parser';
import { CalcController } from './calc.controller';
import { CalcService } from './calc.service';

describe('CalcController', () => {
  let controller: CalcController;

  const mockCalcService = {
    evaluate: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CalcController],
      providers: [
        {
          provide: CalcService,
          useValue: mockCalcService,
        },
      ],
    }).compile();

    controller = module.get<CalcController>(CalcController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should evaluate expression', () => {
    mockCalcService.evaluate.mockReturnValueOnce(2);
    expect(controller.calc('1+1')).toBe(2);
    expect(mockCalcService.evaluate).toBeCalledWith('1+1');
  });

  it('returns 400 error on invalid expression', () => {
    mockCalcService.evaluate.mockImplementationOnce(() => {
      throw new SyntaxError('Invalid syntax');
    });
    expect(() => controller.calc('Invalid expression')).toThrowError(
      new BadRequestException('Invalid syntax'),
    );
  });
});
