import { Injectable } from '@nestjs/common';
import { parse } from './calc-parser';
@Injectable()
export class CalcService {
  evaluate(expression: string): number {
    return parse(expression);
  }
}
