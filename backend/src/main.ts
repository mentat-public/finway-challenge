import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CORS, PORT } from './environment';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  if (CORS) {
    app.enableCors({
      origin: CORS.split(',').map((s) => s.trim()),
      credentials: true,
    });
  }

  await app.listen(PORT);
}
bootstrap();
