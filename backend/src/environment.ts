export const PORT = process.env.PORT ? +process.env.PORT : 3000;
export const MONGO_URL = process.env.MONGO_URL ?? 'mongodb://localhost/finway';
export const CORS = process.env.CORS;
