import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CalcModule } from './calc/calc.module';
import { MONGO_URL } from './environment';

@Module({
  imports: [MongooseModule.forRoot(MONGO_URL), CalcModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
