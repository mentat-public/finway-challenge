import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HistoryDocument = HistoryItem & Document;

@Schema({
  autoIndex: true,
  autoCreate: true,
})
export class HistoryItem {
  @Prop()
  expression: string;

  @Prop()
  value: string;

  @Prop()
  clientId: string;

  @Prop()
  addedOn: Date;
}

export const HistorySchema = SchemaFactory.createForClass(HistoryItem);
HistorySchema.index({ clientId: 1, addedOn: 1 });
