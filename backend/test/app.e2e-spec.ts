import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as mongoose from 'mongoose';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  // Evaluate correct expression
  it('/calc?expression=2*2 (GET)', (done) => {
    request(app.getHttpServer())
      .get('/calc?expression=2*2')
      .expect(200)
      .expect('4')
      .end(() => done());
  });

  // Evaluate wrong expression
  it('/calc?expression=azaza (GET)', (done) => {
    request(app.getHttpServer())
      .get('/calc?expression=azaza')
      .expect(200)
      .expect('{"error":"Expected \\"(\\" or integer but \\"a\\" found."}')
      .end(() => done());
  });

  afterAll(async () => {
    await Promise.all([app.close(), mongoose.disconnect()]);
  });
});
